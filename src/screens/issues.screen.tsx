import React from 'react';
import { setListeners, uploadIssue } from '../providers/issue.provider';
import Issue from "../models/issue.model";
import Dropdown from "react-bootstrap/Dropdown";
import * as Styled from "../styles/styledComponents";

export default function IssuesScreen() {
    const [issues, setIssues] = React.useState([]);
    React.useEffect(() => {
        if (issues.length === 0) {
            //we set the listener from firestore
            setListeners(setIssues)
        }
    }, [issues])
    const addIssue = () => {
        let copiedIssues = JSON.parse(JSON.stringify(issues));
        copiedIssues.unshift(new Issue());
        setIssues(copiedIssues);
    }
    return (
        <div className="App">
            <Styled.Button onClick={addIssue}>
                +
            </Styled.Button>
            <Styled.Table>
                <Styled.Headers>
                    <Styled.Header>TITLE</Styled.Header>
                    <Styled.Header>DESCRIPTION</Styled.Header>
                    <Styled.Header>SEVERITY</Styled.Header>
                    <Styled.Header>STATUS</Styled.Header>
                </Styled.Headers>
                <Styled.Body>
                {issues ? issues.map((el: any, idx: number) => <PrintTable key={"id:" + idx} data={el} even={idx%2==0}/>) : undefined}
                </Styled.Body>
            </Styled.Table>
        </div>
    );
}

function PrintTable(props: { data: Issue,even:Boolean }) {
    const { data } = props;
    return (
        <Styled.Row>
            <Styled.Cell>
                <span>
                <input type="text" value={data.title} onChange={(event) => uploadIssue({ ...data, title: event.target.value })} />
                </span>
            </Styled.Cell>
            <Styled.Cell>
               <span>
                 <input type="text" value={data.description} onChange={(event) => uploadIssue({ ...data, description: event.target.value })} />
               </span>
            </Styled.Cell>
            <Styled.Cell>
                <Dropdown
                    onSelect={(el: string) => uploadIssue({ ...data, severity: el })}>
                    <Dropdown.Toggle variant="warning" id="dropdown-basic">
                        {data.severity}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item eventKey="High">High</Dropdown.Item>
                        <Dropdown.Item eventKey="Medium">Medium</Dropdown.Item>
                        <Dropdown.Item eventKey="Low">Low</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Styled.Cell>
            <Styled.Cell>
                <Dropdown
                    onSelect={(el: string) => uploadIssue({ ...data, status: el })}>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        {data.status}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item eventKey="TODO">TODO</Dropdown.Item>
                        <Dropdown.Item eventKey="DOING">DOING</Dropdown.Item>
                        <Dropdown.Item eventKey="DONE">DONE</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Styled.Cell>
        </Styled.Row>
    )
}
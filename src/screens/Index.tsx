import React from 'react';
import * as firebase from "firebase";
import {firebaseConfig} from "../providers/configuration" 
import IssuesScreen from './issues.screen';

firebase.initializeApp(firebaseConfig)

export default function Index() {
  return (
    <div className="App">
        <IssuesScreen/>
    </div>
  );
}


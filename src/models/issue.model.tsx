export default class Issue{
    id?:string;
    title:string;
    description:string;
    severity:string;
    status:string;
    date:Date;

    constructor(){
        this.title="";
        this.description="";
        this.severity="Severity";
        this.status="Status";
        this.date=new Date()
    }
}
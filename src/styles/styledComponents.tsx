import styled from "styled-components"

export const Table = styled.div`
    width:100%;
    height:100%;
    display:flex;
    flex-direction:column;
    padding:0px 20px;
`

export const Headers = styled.div`
width:100%;
display:flex;
justify-content:space-evenly;
margin-bottom:10px;
padding-bottom:5px;
border-bottom:1px solid #525252;
`
export const Header = styled.div`
 width:20%;
 text-align:center;
 font-weight:Bold;
 color: #525252;
 &:nth-of-type(2){
    width:40%;
}
`
export const Body = styled.div`
display:flex;
flex-direction:column;

`
export const Row = styled.div`
    width:100%;
    display:flex;
    justify-content:space-evenly;
    align-items:center;
    height:40px;
    &:nth-of-type(even){
        background-color:rgba(3,58,255,0.1);
    }
`
export const Cell = styled.div`
    width:20%;
    display:flex;
    justify-content:center;
    padding:0px 5px;
    height:100%;
    &:nth-of-type(2){
        width:40%;
    }
    & input{
        border:none;
        height:100%;
        background:transparent;
        width:100%;
    }
    & span{
        width:100%;
        position:relative;
    }
    & span:after{
        content:"";
        background-image:url(icons/pencil.png);
        display:flex;
        position: absolute;
        right: 0px;
        top: 0px;
        height: 40px;
        width: 20px;
        background-size: contain;
        background-repeat: no-repeat;
        background-position-y: center;
        opacity: 0.3;
    }
`

export const Button = styled.div`
    position:fixed;
    right:20px;
    bottom:20px;
    background-color:#525252;
    color:white;
    border-radius:100%;
    width:40px;
    height:40px;
    display:flex;
    justify-content:center;
    line-height:32px;
    font-size:40px;
    box-shadow:2px 2px 5px 0px rgba(0,0,0,0.5);
    &:hover{
        cursor:pointer;
        background-color:#626262;
    }
    &:active{
        box-shadow:inset 2px 2px 5px 0px rgba(0,0,0,0.5);
    }
`
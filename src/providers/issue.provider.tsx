import { firestore } from "firebase";
import Issue from "../models/issue.model";

export function setListeners(setState:(state:any)=>void){
   firestore().collection("issues").onSnapshot({
       error:console.log,
       next:(snapshot=>{
           const response:Array<any>=[];
           for(let  doc of snapshot.docs){
            response.push({...doc.data(),id:doc.id});
           }
           response.sort((a,b)=>{
               let aDate = new Date(a.date);
               let bDate = new Date(b.date);
               if(aDate.getTime()>bDate.getTime())
               return -1;
               else if(aDate.getTime()<bDate.getTime())
               return 1;
               else
               return 0;
           })
           setState(response);
       }),
   })
}

export function uploadIssue(issue:Issue):Promise< void | firestore.DocumentReference<firestore.DocumentData> >{
    const copiedIssue = JSON.parse(JSON.stringify(issue))
    if(copiedIssue.id){
        let id = copiedIssue.id;
        delete copiedIssue.id;
        return firestore().collection('issues').doc(id).set(copiedIssue);
    }else{
        return firestore().collection('issues').add(copiedIssue);
    }
}
